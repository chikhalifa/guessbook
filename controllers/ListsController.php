<?php

namespace app\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use app\models\Lists;
class ListsController extends \yii\web\Controller
{
        
public function actionCreate()
{
    $model = new Lists();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            // Save the form value
            $model->save();
            // show message
             Yii::$app->getSession()->setFlash('success' , 'Guest List Has added successfully');
             //redirect
            return $this->redirect('/guestbook/web/index.php?r=lists');
            return;
        }
    }

    return $this->render('create', [
        'model' => $model,
    ]);
}

    public function actionDelete($id)
    {
                 $model = Lists::findOne($id);
                 $model->delete();

        // show message
             Yii::$app->getSession()->setFlash('success' , 'Guestlist Has deleted successfully');
             //redirect
            return $this->redirect('/guestbook/web/index.php?r=lists');    }
    
    public function actionDetails($id)
 
    {
        $list = Lists::find()
        ->where(['id'=>$id])
        ->one();

        //render the view 
        return $this->render('details', ['list'=>$list]);
    }
   public function actionEdit($id)
    {
         $model = Lists::findOne($id);

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            // Save the form value
            $model->save();
            // show message
             Yii::$app->getSession()->setFlash('success' , 'Guestlist Has edited successfully');
             //redirect
            return $this->redirect('/guestbook/web/index.php?r=lists');
            return;
        }
    }

    return $this->render('edit', [
        'model' => $model,
    ]);

       
    }
    
     public function actionIndex()
    {
        // Create Query
        $query = Lists::find();

        
          $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $list = $query->orderBy('created_date DESC')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'lists' => $list,
            'pagination' => $pagination,
        ]);

    }
}