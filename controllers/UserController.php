<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use app\models\User;

class UserController extends Controller
{
    public function actionLogin()
    {
        return $this->render('login');
    }

   
public function actionRegister()
{
    $model = new User();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->validate()) {
            // form inputs are valid, do something here
            $model->save();
            // show success message
            Yii::$app->getSession()->setFlash('success' , 'Your Registration was successful');
            return $this->redirect('/guestbook/web/index.php');
            
        }
    }
	
    return $this->render('register', [
        'model' => $model,
    ]);
}



}
