 <?php
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<a href="/guestbook/web/index.php?r=lists">Back to Guest List</a>
<h2 class="page-header"><strong><li class="list-group-item  "><a href="/guestbook/web/index.php?r=lists/details&id=<?php echo $list->id ?>"> <?=$list->full_name?> </a></strong><small> From <?php echo $list->states; ?></small> <span class ="pull-right">
<a class="btn btn-primary" href="index.php?r=lists/edit&id=<?php echo $list->id; ?>">Edit</a><a class="btn btn-danger" href="index.php?r=lists/delete&id=<?php echo $list->id; ?>">Delete</a>

</span>
</h2>
<?php if(!empty($list->event_category)) : ?>
<div class="well 
   style= 'background-color: #b0e0e6'
">
<h4>GUEST DESCRIPTION TYPE</h4>
<strong><?php echo $list->event_category; ?></strong>

</div>
<?php endif;?>
<ul class="list-group">

	<?php if(!empty($list->created_date)) :?>
		<?php $phpdate = strtotime($list->created_date);?>
		<?php $formatted_date = date("F j, Y , g:i a", $phpdate);?>
		<li class="list-group-item"><strong>Listing Date:</strong><?php echo $list->created_date;?></li>

<?php endif;?>

<?php if(!empty($list->full_name)) :?>
		
		<li class="list-group-item"><strong>Guest Name:</strong><?php echo $list->full_name;   ?></li>

<?php endif; ?>

	<?php if(!empty($list->email)) :?>
		
		<li class="list-group-item"><strong>Contact Email:</strong><?php echo $list->email;   ?></li>

<?php endif; ?>

		<?php if(!empty($list->states)) :?>
		
		<li class="list-group-item"><strong>State Of Event:</strong><?php echo $list->states;   ?></li>

<?php endif; ?>

		<?php if(!empty($list->event_category)) :?>
		
		<li class="list-group-item"><strong>Event Category:</strong><?php echo $list->event_category;   ?></li>

<?php endif; ?>
	

		<?php if(!empty($list->seat_number)) :?>
		
		<li class="list-group-item"><strong>Seat Number:</strong><?php echo $list->seat_number;   ?></li>

<?php endif; ?>


</ul>
<a class="btn btn-primary" href="mailto:<?php echo $list->email;?>?Subject=List%20Application">Contact Guest</a>